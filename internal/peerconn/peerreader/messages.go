package peerreader

import (
	"gitlab.com/xoka/xtor/internal/bufferpool"
	"gitlab.com/xoka/xtor/internal/peerprotocol"
)

// Piece message that is read from peers.
// Data of the piece is wrapped with a bufferpool.Buffer object.
type Piece struct {
	peerprotocol.PieceMessage
	Buffer bufferpool.Buffer
}
